﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    private static GameManager instance;

    public TankData data;
    public GameObject player;
    public GameObject[] enemies;
    public GameObject[] spawnPoints;
    private Vector3 respawnLocation;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        } else
        {
            Destroy(gameObject);
        } //Make Singleton

    }

    void Start()
    {
        respawnLocation = player.transform.position;  
        //SpawnPlayer();   
    }

    void Update()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("PSpawn");
        enemies = GameObject.FindGameObjectsWithTag("EnemyTank"); //End game if enemies array = 0
        if (enemies.Length == 0)
        {
            SceneManager.LoadScene(0);
        }
    }

    /*private void SpawnPlayer()
    {
        int spawn = Random.Range(0, spawnPoints.Length);
        GameObject.Instantiate(player, spawnPoints[spawn].transform.position, Quaternion.identity);
    }*/

}
