﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public int mapSeed;

    public int rows;
    public int cols;

    private float roomWidth = 50f;
    private float roomHeight = 50f;
    private Room[,] grid;
    public GameObject[] gridPrefabs;
    
    public bool isMapOfTheDay;
    public bool isRandomMap;

    void Start()
    {
        if(isMapOfTheDay)
        {
            mapSeed = DateToInt(DateTime.Now.Date);
        }
        if(isRandomMap)
        {
            mapSeed = DateToInt(DateTime.Now);
        }
        GenerateGrid();
    }

    public void GenerateGrid()
    {
        UnityEngine.Random.InitState(mapSeed);
        grid = new Room[cols,rows];

        for(int i = 0; i < rows; i++)
        {
            for(int j = 0; j < cols; j++)
            {
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0f, zPosition);

                GameObject tempRoomObj = Instantiate (RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;

                tempRoomObj.transform.parent = this.transform;

                tempRoomObj.name = "Room_"+j+","+i;
                
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                //Open The Doors
                //North Doors
                if(i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                } else if (i == rows-1)
                {
                    tempRoom.doorSouth.SetActive(false);
                } else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                if(j == 0)
                {
                    tempRoom.doorEast.SetActive(false); 
                } else if (j == cols-1)
                {
                    tempRoom.doorWest.SetActive(false);
                } else
                {
                    tempRoom.doorEast.SetActive(false); 
                    tempRoom.doorWest.SetActive(false);
                }
                //Save to the grid array
                grid[j,i] = tempRoom;
            }
        }
    }

    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0,gridPrefabs.Length)];
    }

    public int DateToInt (DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }
}
