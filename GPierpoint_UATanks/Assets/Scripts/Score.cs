﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
        public Text scoreText;
        public int score;


    void Update()
    {
        scoreText.text = "Score: " + score;

        if(Input.GetKeyDown(KeyCode.Space))
        {
            score += 100;
        }
    }
}
