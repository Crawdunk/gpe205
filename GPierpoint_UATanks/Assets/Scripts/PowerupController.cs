﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    public TankData data;
    public List<Powerup> powerups;

    void Start()
    {
        powerups = new List<Powerup>();
        data = GameObject.FindWithTag("Tank").GetComponent<TankData>();
    }

    void Update()
    {
        List<Powerup> expiredPowerups = new List<Powerup>();

        foreach (Powerup power in powerups)
        {
            power.duration -=Time.deltaTime;

            if(power.duration <=0)
            {
            expiredPowerups.Add (power);
            }
        }

        foreach (Powerup power in expiredPowerups)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }
        expiredPowerups.Clear();
    }

    public void Add(Powerup powerup)
    {
        powerup.OnActivate(data);

        if(!powerup.isPermanent)
        {
            powerups.Add (powerup);
        }
    }
}
