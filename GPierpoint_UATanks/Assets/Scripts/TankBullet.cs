﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBullet : MonoBehaviour
{
    public TankData data;
    public AIController ai;
    public TestController test;
    public GameObject enemyTank;
    public AudioSource impact;

    void Awake()
    {
        impact = GetComponent<AudioSource>();
    }

    void Start()
    {
        //ai = GameObject.FindWithTag("EnemyTank").GetComponent<AIController>(); //Find the AIController
        test = GameObject.FindWithTag("Tank").GetComponent<TestController>(); //Find the TestController     
    }

    void OnCollisionEnter(Collision other)
    {
        impact.Play(0); 
        if(other.gameObject.CompareTag("EnemyTank"))
        {
            ai.TakeDamage(); //Call the TakeDamage function from the AIController.
            test.score += data.damagePoints; //Add damagePoints(Set in TankData) to your total score.
        }    
        
        Destroy(this.gameObject); //Destroy the bullet.
    }    
}
