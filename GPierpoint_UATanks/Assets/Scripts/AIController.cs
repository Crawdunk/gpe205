﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State { Patrol, Look, Chase, Flee }

public class AIController : MonoBehaviour
{
    public TankPatrol patrol;
    public TankData data;
    public TankMotor motor;
    public TestController test;
    public float shootRange;
    private float dist;
    public Transform player;
    public float speed = 5f;
    public bool isPatrolling;
    public bool isTurning;
    public bool canSeePlayer;
    public State state;
    public float rotationLeft = 360;

    // Start is called before the first frame update
    void Start()
    {
        data.currentHealth = data.maxHealth; //Set currentHealth to maxHealth
        player = GameObject.FindWithTag("Tank").GetComponent<Transform>(); //Find the Player Transform.
        test = GameObject.FindWithTag("Tank").GetComponent<TestController>(); //Find the testController.
        state = State.Patrol;   
        isPatrolling = true;
        canSeePlayer = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Distance
        dist = Vector3.Distance(player.position, transform.position); //Find the distance between the enemyTank position and the player position.
        float turnRotation = data.turnSpeed * Time.deltaTime;

        if(data.currentHealth <= 0)
        {
            this.gameObject.SetActive(false); //Disable the gameObject when health is less than or equal to 0
            test.score += data.destroyPoints; //Add destroyPoints to the player's total score.
        }
        if(dist <= shootRange && canSeePlayer == true) //If the distance to the player is less than the shootRange.
        {
            //Direction/Rotation
            Vector3 direction = player.position - motor.mCannon.position; //Find the direction that the player is from the enemyTank turret.
            Quaternion rotation = Quaternion.LookRotation(direction); //Set the Quaternion rotation to the LookRotation towards the direction set before.
            motor.mCannon.rotation = Quaternion.Lerp(motor.mCannon.rotation, rotation, speed * Time.deltaTime); //Rotation the canon towards the rotation Quaternion WITH a slight delay set by the speed multiplied by deltaTime.
        }

        if(dist <= shootRange && Time.time > data.nextFire && canSeePlayer == true) //If the dist is less than the shootRange and Time.time is greater than nextFire.
        {
            data.nextFire = Time.time + data.fireRate; //See TankMotor
            motor.Shoot(data.bulletForce); //This is the same shoot function from the TankMotor.
        }

        //States
        if (state == State.Look)
        {
            if(rotationLeft > turnRotation)
            {
                rotationLeft -= turnRotation;
                isTurning = true;
            } else
            {
                turnRotation = rotationLeft;
                rotationLeft = 0;
                isTurning = false;
            }
            transform.Rotate(0,turnRotation,0);
        } 


        if (dist <= data.hearingRange)
        {
            state = State.Look;
            isPatrolling = false;
        } else
        {
            state = State.Patrol;
            isPatrolling = true;
        }

        if(isPatrolling == true)
        {
            if(patrol.waypointDist < 0.5f)
            {
                patrol.IncreaseIndex();
            } else
            {
                patrol.Patrol();
            }
            
        } 

    }

    public void TakeDamage()
    {
        data.currentHealth -= data.bulletDamage; //Subtract bulletDamage from the currentHealth of the tank.
    }
}
