﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{

    private CharacterController characterController;
    public Transform tf;
    public Transform turretTf;
    float speed;

    public Rigidbody shell;
    public Transform fireStart;
    public Transform mCannon;

    public AudioSource bullet;

    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>(); //Grabs the transform of the GameObject this is on.
    }

    void Start()
    {
        characterController = gameObject.GetComponent<CharacterController>(); //Identify and set the character controller.
        mCannon = fireStart.parent; //Set the cannon to the parent object of the fireStart GameObject.
        bullet = GetComponent<AudioSource>();
    }

    public void Move(float speed)
    {
            if(Input.GetKey(KeyCode.W)) 
            {
                Vector3 forwardVector; //Create a new vector 3

                forwardVector = tf.forward * speed; //Set speedVector to be transform.forward TIMES speed, therefore you move FORWARD at the set amount of frames per second/or time with SimpleMove

                characterController.SimpleMove (forwardVector); //Call SimpleMove and give it to our vector to use.
            }    
    }

    public void Reverse(float speed)
    {
        if (Input.GetKey(KeyCode.S))
        {
            Vector3 backVector; //New Vector3

            backVector = -tf.forward * speed; //Set it to be the reverse of forward.

            characterController.SimpleMove (backVector); //Call SimpleMove and give it to our vector to use.
        }
    }

    public void Rotate(float speed)
    {  
        if(Input.GetKey(KeyCode.D)) 
            {
                Vector3 rotateVector; //Create a new vector 3 called rotateVector

                rotateVector = Vector3.up * speed * Time.deltaTime; //Set it equal to a Vector3.up Multiplied by Speed Multiplied by Time.deltaTime to set it to seconds instead of frames.

                tf.Rotate (rotateVector, Space.Self); //Call your vector and have it set to your GAME OBJECTS rotation/space instead of the world's space
            }    

        if(Input.GetKey(KeyCode.A)) 
            {
                Vector3 rotateVector; //Create a new vector 3 called rotateVector

                rotateVector = Vector3.down * speed * Time.deltaTime; //Set it equal to a Vector3.down Multiplied by Speed Multiplied by Time.deltaTime to set it to seconds instead of frames.

                tf.Rotate (rotateVector, Space.Self); //Call your vector and have it set to your GAME OBJECTS rotation/space instead of the world's space
            }  
    }

    public void Turret(float speed)
    {
        if(Input.GetKey(KeyCode.E)) 
            {
                Vector3 turretVector; //Make a new Vector3

                turretVector = Vector3.up * speed * Time.deltaTime; //Set it equal to Vector3.up * speed * Time.deltaTime.

                turretTf.Rotate(turretVector, Space.Self); //Make the transform of the turret ONLY to rotate.
            }    

        if(Input.GetKey(KeyCode.Q)) 
            {
                Vector3 turretVector; //Make a new Vector3

                turretVector = Vector3.down * speed * Time.deltaTime; //Set it equal to Vector3.down * speed * Time.deltaTime.

                turretTf.Rotate(turretVector, Space.Self); //Make the transform of the turret ONLY to rotate.
            }         
    }

    public void Shoot(float speed)
    {
            Rigidbody rbShell = Instantiate(shell, fireStart.position, mCannon.rotation); //Create a new Rigidbody and set it to be an Instantiated shell, at the fireStart position and the rotation of mCannon.
            bullet.Play();
            rbShell.velocity = speed * mCannon.forward; //Set rbShell's velocity to speed * mCannon.forward so it moves in the direction that mCannon is facing.
    }

    public void RotateTowards(Transform target, float speed)
    {
        Vector3 direction = target.position - tf.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        tf.rotation = Quaternion.RotateTowards(tf.rotation, rotation, speed * Time.deltaTime); 
    }
}
