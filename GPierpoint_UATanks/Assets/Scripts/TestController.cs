﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{
    public TankMotor motor;
    public TankData data;
    public float score;
    float damage;

    // Start is called before the first frame update
    void Start()
    {
        data.currentHealth = data.maxHealth; //Set currentHealth to maxHealth
    }

    // Update is called once per frame
    void Update()
    {
        //Movement
        motor.Move(data.moveSpeed); //Use the Move function with the moveSpeed set in TankData.   
        motor.Rotate(data.turnSpeed); //Use the Rotate function with the turnSpeed set in TankData.   
        motor.Turret(data.turretSpeed); //Use the Turret function with the turretSpeed set in TankData.   
        motor.Reverse(data.reverseSpeed); //Use the Reverse function with the reverseSpeed set in TankData.   

        //Shooting
        if(Input.GetKeyDown(KeyCode.Space) && Time.time > data.nextFire) //If Space is pressed and Time.time is greater than the nextFire float.
        {
            data.nextFire = Time.time + data.fireRate; //nextFire is equal to Time.time plus the fireRate set in TankData.
            motor.Shoot(data.bulletForce); //Use the Shoot function with the bulletForce set in TankData.
        }

        //Death
        if(data.currentHealth <= 0)
        {
            Destroy(this.gameObject); //If currentHealth is less than or equal to 0, destroy the GameObject.
        }
    }

    public void TakeDamage(float damage)
    {
        data.currentHealth -= damage; //Take your current health and subtract it by the damage set in TankData.
    }
}
