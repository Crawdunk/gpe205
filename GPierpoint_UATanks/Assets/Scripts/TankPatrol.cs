﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankPatrol : MonoBehaviour
{
    public AIController ai;
    public TankData data;
    public TankMotor motor;
    public Transform[] waypoints;
    private int waypointIndex;
    public float waypointDist;

    // Start is called before the first frame update
    void Start()
    {
        waypointIndex = 0;
        transform.LookAt(waypoints[waypointIndex].position);
    }

    // Update is called once per frame
    void Update()
    {
        waypointDist = Vector3.Distance(transform.position, waypoints[waypointIndex].position);
    }

    public void Patrol()
    {
        transform.Translate(Vector3.forward * data.moveSpeed * Time.deltaTime);
    }

    public void IncreaseIndex()
    {
        waypointIndex++;
        if(waypointIndex >= waypoints.Length)
        {
            waypointIndex = 0;
        }
        transform.LookAt(waypoints[waypointIndex].position);
    }
}
