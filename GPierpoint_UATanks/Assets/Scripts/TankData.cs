﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    //Add all the data being used in other scripts to be edited from here/the inspector.
    public float moveSpeed = 3f;
    public float reverseSpeed = 1.5f;
    public float turnSpeed = 180f;
    public float turretSpeed = 100f;
    public float fireRate = 1f;
    public float nextFire = 0f;
    public float bulletForce = 30f;

    public float maxHealth = 100f;
    public float currentHealth;
    public float bulletDamage = 10f;
    public float destroyPoints = 100f;
    public float damagePoints = 10f;

    public float hearingRange = 15f;

}
