﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Powerup powerup;
    public AudioClip feedback;

    public void OnTriggerEnter(Collider other)
    {
        PowerupController powCon = other.GetComponent<PowerupController>();

        if(powCon != null)
        {
            powCon.Add(powerup);

            if(feedback != null)
            {
                AudioSource.PlayClipAtPoint(feedback, transform.position, 1.0f);
            }

            Destroy (this.gameObject);
        }
    }
}
