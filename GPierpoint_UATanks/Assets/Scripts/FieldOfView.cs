﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public Transform player;

    //AI Sight
    public bool playerIsInLOS = false;
    public float fieldOfViewAngle = 160f;
    public float losRadius = 45f;

    //AI Memory
    private bool aiMemorizesPlayer = false;
    public float memoryStartTime = 10f;
    private float increasingMemoryTime;

    //AI Hearing
    Vector3 noisePosition;
    private bool aiHeardPlayer = false;
    public float noiseTravelDistance = 50f;

    void Awake()
    {
        player = GameObject.FindWithTag("Tank").GetComponent<Transform>();
    }

    void Update()
    {
        float distance = Vector3.Distance(player.position, transform.position);
        if(distance <= losRadius)
        {
            CheckLOS();
        }

        if(playerIsInLOS == false && aiMemorizesPlayer == false && aiHeardPlayer == false)
        {
            //Patrol();
            //NoiseCheck();

            StopCoroutine(AiMemory());
        }
        else if (aiHeardPlayer == true && playerIsInLOS == false && aiMemorizesPlayer == false)
        {
            //GoToNoisePosition();
        }
        else if (playerIsInLOS == true)
        {
            aiMemorizesPlayer = true;

            //FacePlayer();

            //ChasePlayer();
        }
        else if (aiMemorizesPlayer == true && playerIsInLOS == false)
        {
            //ChasePlayer();

            StartCoroutine(AiMemory());
        }
    }

    void NoiseCheck()
    {
        float distance = Vector3.Distance(player.position, transform.position);

        if(distance <= noiseTravelDistance)
        {
            if(Input.GetButton("Fire1"))
            {
                noisePosition = player.position;
                aiHeardPlayer = true;
            }
            else
            {
                aiHeardPlayer = false;
            }
        }
    }

    void CheckLOS()
    {
        Vector3 direction = player.position - transform.position;

        float angle = Vector3.Angle(direction, transform.forward);

        if(angle < fieldOfViewAngle * 0.5f)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, direction.normalized, out hit, losRadius))
            {
                if (hit.collider.tag == "Tank")
                {
                    playerIsInLOS = true;
                    aiMemorizesPlayer = true;
                }
                else
                {
                    playerIsInLOS = false;
                }
            }
        }
    }

    IEnumerator AiMemory()
    {
        increasingMemoryTime = 0;

        while (increasingMemoryTime < memoryStartTime)
        {
            increasingMemoryTime += Time.deltaTime;
            aiMemorizesPlayer = true;
            yield return null;
        }

        aiHeardPlayer = false;
        aiMemorizesPlayer = false;
    }

}
